FROM node:lts-jessie

RUN apt-get update \
    && apt-get install -y \
    redis-server mongodb \
    && rm -rf /var/lib/apt/lists/*

ADD . /app

WORKDIR /app

ENV NODE_ENV=production

COPY package.json ./

RUN npm i

COPY . .

EXPOSE 4000

CMD [ "node", "dist/server" ]