const isDev = process.env.NODE_ENV !== "production";

module.exports = {
  apps: [
    {
      name: "webwiz-core",
      script: "node ./dist/server",
      dev: false,
      prod: true,
    },
    {
      name: "redis-server",
      script: "redis-server",
      dev: true,
      prod: true,
    },
    {
      name: "mongo-db",
      script: "mongod",
      dev: true,
      prod: true,
    },
  ].filter((app) => app.dev === isDev || app.prod === !isDev),
};
