import { gql } from "apollo-server";

export default gql`
  type User {
    email: String
    verified: Boolean
    oauth: [OAuth]
  }

  type OAuth {
    provider: String
    id: String
  }

  type Token {
    token: String
  }

  type Website {
    host: String
    verified: Boolean
    modules: [String]
    keys: [String]
    admins: [User]
    users: [User]
  }

  type Query {
    # User queries
    user(token: String!): User

    # Webiste queries
    websites(token: String!): [Website] 
    website(token: String!, host: String!): Website
  }

  type Mutation {
    # User mutations
    createUserWithPassword(email: String!, password: String!): User
    signInWithPassword(email: String!, password: String!): Token

    # Website mutations
    createWebsite(host: String!, token: String!): Website
  }
`;
