import { AuthenticationError } from "apollo-server";
import { connect, Db, ObjectId } from "mongodb";
import { createClient, RedisClient } from "redis";

let DB: Db;

export const redis: RedisClient = createClient({
  host: process.env.NODE_ENV !== "production" ? "127.0.0.1" : "redis",
});

export const db = () => DB;

export const detokenizeId = (token: string) => {
  return new Promise((resolve, reject) => {
    redis.get(token, (err, user_id) => {
      if (err) reject(err);
      if (!user_id) reject(new AuthenticationError("Invalid or Expired Token"));
      const _id = new ObjectId(user_id);
      resolve(_id);
    });
  });
};

export const detokenizeUser = (token: string) => {
  return new Promise((resolve, reject) => {
    redis.get(token, (err, user_id) => {
      if (err) reject(err);
      if (!user_id) reject(new AuthenticationError("Invalid or Expired Token"));
      db()
        .collection("users")
        .findOne({ _id: new ObjectId(user_id) })
        .then((user) => {
          if (!user) throw new AuthenticationError("User not found");
          resolve(user);
        })
        .catch((err) => {
          reject(err);
        });
    });
  });
};

connect(
  process.env.NODE_ENV !== "production"
    ? "mongodb://localhost"
    : "mongodb://mongo",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
)
  .then(async (client) => {
    DB = client.db("webwiz-core");

    // DB inits
    client
      .db("webwiz-core")
      .collection("users")
      .createIndex("email", { unique: true });
    client
      .db("webwiz-core")
      .collection("websites")
      .createIndex("host", { unique: true });
  })
  .catch((e) => {
    throw e;
  });
