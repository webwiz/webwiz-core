import { UserInputError, ForbiddenError } from "apollo-server";
import validator from "validator";

import { db, detokenizeUser } from "../db";

export default {
  Query: {},
  Mutation: {
    createWebsite: async (_, { token, host }) => {
      if (!validator.isURL(host)) throw new UserInputError("Invalid Host");
      const user = await detokenizeUser(token);
      //todo user interface
      // @ts-ignore
      if (!user.verified) {
        throw new ForbiddenError(
          "Only verified users are allowed to create a website"
        );
      }
      const website = await db()
        .collection("websites")
        .insertOne({
          host,
          verified: false,
          // @ts-ignore
          admins: [user._id],
        });
      return website.ops[0];
    },
  },
};
