import user from "./users";
import websites from "./websites";

export default {
  Query: {
    ...user.Query,
    ...websites.Query,
  },
  Mutation: {
    ...user.Mutation,
    ...websites.Mutation,
  },
};
