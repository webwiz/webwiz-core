import { UserInputError, AuthenticationError } from "apollo-server";
import validator from "validator";
import { hashSync, compareSync } from "bcrypt";
import { randomBytes } from "crypto";
import { ObjectId } from "mongodb";

import { db, redis, detokenizeUser } from "../db";

export default {
  Query: {
    user: async (_, { token }) => {
      return await detokenizeUser(token);
    },
  },
  Mutation: {
    createUserWithPassword: async (_, { email, password }) => {
      try {
        if (!validator.isEmail(email)) {
          throw new UserInputError("Bad Email");
        }
        password = hashSync(password, 10);
        const user = await db().collection("users").insertOne({
          email,
          password,
          verified: false,
        });
        return { ...user.ops[0] };
      } catch (e) {
        throw new UserInputError(e.message);
      }
    },
    signInWithPassword: async (_, { email, password }) => {
      try {
        const user = await db().collection("users").findOne({ email });
        if (!user) throw new UserInputError("No such user");
        const match = compareSync(password, user.password);
        if (!match) throw new UserInputError("Wrong Password");
        const token = randomBytes(64).toString("hex");
        redis.SETEX(token, 1000 * 60 * 60 * 24 * 7, user._id.toString());
        return { token };
      } catch (e) {
        if (e instanceof UserInputError) throw e;
        throw new AuthenticationError(e.message);
      }
    },
  },
};
